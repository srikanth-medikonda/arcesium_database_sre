**Problem Statement:**

1. GitHub parser using python

2. Deliverable is to detect work load change or query regression

   - Work load change - Number of queries are changed in a session
   - Query regression - Query taking x amount of time, is now taking y amount of time where y >= x + z%

3. Dump actual execution plan with I/O time

4. For each query, get:
    - Duration
    - Logical Reads
    - Physical Reads
    - I/O time
    - CPU time
    - Query text with actual parameters
    - Query plan
