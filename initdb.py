import os

DATADIR = ("/tmp/arcesium")
CHECK_IF_FOLDER_EXISTS = os.path.isdir(DATADIR)

#If DATADIR already exist, exit the program
if CHECK_IF_FOLDER_EXISTS:
    print(DATADIR, "Data directory folder already exists, exiting the program.")
    exit()

#Initialize PostgreSQL database cluster
os.system('pg_ctl -D %s initdb' % DATADIR)

#Modify HBA config parameters
hba_config_params = open('%s/pg_hba.conf' % DATADIR, 'a')
hba_config_params.writelines(
    ["host    all             all             0.0.0.0/0               md5\n"]
)
hba_config_params.close()

#Modify DB config parameters
db_config_params = open('%s/postgresql.conf' % DATADIR, 'a')
db_config_params.writelines(
    ["auto_explain.log_analyze = 1\n",
    "auto_explain.log_buffers = 1\n",
    "auto_explain.log_timing = 1\n",
    "auto_explain.log_triggers = 1\n",
    "auto_explain.log_verbose = 1\n",
    "auto_explain.log_format = json\n",
    "auto_explain.log_min_duration = 0\n",
    "auto_explain.log_nested_statements = 1\n",
    "auto_explain.sample_rate = 1\n",
    "log_checkpoints = on\n",
    "log_connections = on\n",
    "log_destination = 'csvlog'\n",
    "log_disconnections = on\n",
    "log_duration = on\n",
    "log_directory = '/tmp/log'\n",
    "log_filename = 'postgresql-%Y-%m-%d.log'\n",
    "log_lock_waits = on\n",
    "log_statement = 'all'\n",
    "shared_preload_libraries = 'auto_explain, pg_stat_statements'\n",
    "listen_addresses = '*'\n",
    "port = 12345\n"]
)
db_config_params.close()

#Start PostgreSQL database cluster
os.system('pg_ctl -D %s start' %DATADIR)

#Populate sample data, using pgbench
os.system('pgbench -i -s 50 -p 12345 postgres')
