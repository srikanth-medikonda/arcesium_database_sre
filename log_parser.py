"""
*Problem statement:*
1. GitHub parser using python
2. Deliverable is to detect work load change or query regression
   a. Work load change - Number of queries are changed in a session
   b. Query regression - Query taking x amount of time, is now taking y amount of time where y >= x + z%
3. Dump actual execution plan with I/O time
4. For each query, get
    - Duration
    - Logical Reads
    - Physical Reads
    - I/O time
    - CPU time
    - Query text with actual parameters
    - Query plan
"""

"""
#Initiate PostgreSQL database cluster
pg_ctl -D /tmp/arcesium initdb

#Modify parameters
echo "
auto_explain.log_analyze = 1
auto_explain.log_buffers = 1
auto_explain.log_timing = 1
auto_explain.log_triggers = 1
auto_explain.log_verbose = 1
auto_explain.log_format = json
auto_explain.log_min_duration = 0
auto_explain.log_nested_statements = 1
auto_explain.sample_rate = 1
log_checkpoints = on
log_connections = on
log_destination = 'csvlog'
log_disconnections = on
log_duration = on
log_directory = '/tmp/arcesium/log'
log_filename = 'postgresql-%Y-%m-%d.log'
log_lock_waits = on
log_statement = 'all'
shared_preload_libraries = 'auto_explain, pg_stat_statements'
listen_addresses = '*'
port = 12345
" | tee -a /tmp/arcesium/postgresql.conf

echo "
host    all             all             0.0.0.0/0               md5
" | tee -a /tmp/arcesium/pg_hba.conf

#Start PostgreSQL database cluster
pg_ctl -D /tmp/arcesium start

#Populate sample data, using pgbench
export PGPORT=12345
pgbench -i -s 50 postgres

#Create additional user for query purpose
psql -d postgres <<EOF
CREATE EXTENSION IF NOT EXISTS pg_stat_statements;
CREATE USER srikanth ENCRYPTED PASSWORD 'srikanth123';
GRANT CONNECT ON DATABASE postgres TO srikanth;
GRANT USAGE ON SCHEMA public TO srikanth;
GRANT SELECT ON ALL TABLES IN SCHEMA public TO srikanth;
CREATE TABLE IF NOT EXISTS parsed_info
(
cpu_time text,
logical_reads text,
query_text text,
database_name text,
physical_reads text,
session_id text,
application_name text,
io_time text,
-- Postgres Explain Visualizer 2: https://dalibo.github.io/pev2/#/
original_plan text,
connection_from text,
masked_plan_md5_value text,
user_name text,
session_start_time text,
duration text
);
EOF

#Sample database queries
export PGPASSWORD=srikanth123
psql -d postgres -U srikanth <<EOF
SELECT count(*) FROM pgbench_accounts;
SELECT count(*) FROM pgbench_branches;
SELECT count(*) FROM pgbench_history;
SELECT count(*) FROM pgbench_tellers;
SELECT count(*) FROM pgbench_accounts WHERE aid = 1;
SELECT count(*) FROM pgbench_accounts WHERE aid = 100;
SELECT count(*) FROM pgbench_accounts WHERE aid > 100;
EOF
"""

import json
import csv
import sys
import hashlib

input_csv_file = sys.argv[1]
output_csv_file = sys.argv[2]
input_csv_columns = ['log_time','user_name','database_name','process_id','connection_from','session_id','session_line_num','command_tag','session_start_time','virtual_transaction_id','transaction_id','error_severity','sql_state_code','message','detail','hint','internal_query','internal_query_pos','context','query','query_pos','location','application_name']
output_csv_columns = ['cpu_time','logical_reads','query_text','database_name','physical_reads','session_id','application_name','io_time','original_plan','connection_from','masked_plan_md5_value','user_name','session_start_time','duration']
with open(output_csv_file, 'w') as csvfile:
    writer = csv.DictWriter(csvfile, fieldnames=output_csv_columns)
    writer.writeheader()
    csvfile.close()

with open(input_csv_file) as f:
    cf = csv.DictReader(f, fieldnames=input_csv_columns)
    isquerySelected = False
    for row in cf:
        messageData = row['message'].split('\n')
        if(messageData[0].startswith('duration:') and messageData[0].endswith('plan:') and isquerySelected == False):
            isquerySelected = True;
            metricData = json.loads("\n".join(messageData[1:]));
            per_query_dict = {}
            per_query_dict['session_id'] = row['session_id']
            per_query_dict['database_name'] = row['database_name']
            per_query_dict['user_name'] = row['user_name']
            per_query_dict['connection_from'] = row['connection_from']
            per_query_dict['session_start_time'] = row['session_start_time']
            per_query_dict['application_name'] = row['application_name']
            per_query_dict['original_plan'] = json.dumps(metricData)
            per_query_dict['logical_reads'] = metricData['Plan']['Shared Hit Blocks']
            per_query_dict['physical_reads'] = float(metricData['Plan']['Shared Read Blocks'])
            per_query_dict['io_time'] = metricData['Plan']['Actual Total Time']
            per_query_dict['query_text']= json.dumps(metricData['Query Text'])
            #Mask plan values
            for k,v in metricData['Plan'].items():
                metricData['Plan'][k]='xxxx'
            per_query_dict['masked_plan_md5_value'] = hashlib.md5(json.dumps(metricData['Plan'], sort_keys=True)).hexdigest()
            continue;

        if(isquerySelected == True):
            per_query_dict['duration'] = float(row['message'].split('duration: ')[-1].rstrip(' ms'))
            per_query_dict['cpu_time'] = per_query_dict['duration'] - per_query_dict['io_time']
            isquerySelected =  False
            try:
                with open(output_csv_file, 'a+') as csvfile:
                    writer = csv.DictWriter(csvfile, fieldnames=output_csv_columns)
                    writer.writerow(per_query_dict)
                    csvfile.close()
            except IOError:
                print("I/O error")

"""
#Script execution example:
python arceisum.py postgresql-2020-04-08.csv output.csv

#Load parsed info into database table:
psql -d postgres <<EOF
\COPY parsed_info FROM output.csv CSV HEADER;
EOF

#Stop PostgreSQL database cluster:
pg_ctl -D /tmp/arcesium/ stop

#Destroy data directory:
rm -r /tmp/arcesium
"""
